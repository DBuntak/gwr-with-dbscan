package hr.fer.tel.spark.ml.gwr.cluster

import hr.fer.tel.spark.ml.gwr.cluster
import org.apache.spark.ml.clustering.{KMeans, KMeansModel}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.sql.{DataFrame, SparkSession}

class GeospatialKMeans(val templateModel: KMeans)(implicit spark: SparkSession)
  extends BaseGeospatialClustering {

  override protected def fitInternal(df: DataFrame,
                                     centroidCol: String,
                                     clusterCol: String): GeospatialClusteringModel = {
    val tempModel = templateModel.copy(ParamMap.empty)
    tempModel.setFeaturesCol(centroidCol)
    tempModel.setPredictionCol(clusterCol)

    new GeospatialKMeansModel(tempModel.fit(df))
  }
}

object GeospatialKMeansModel extends GeospatialClusteringModelLoader {

  override def load(path: String)(implicit spark: SparkSession): GeospatialKMeansModel = {
    new cluster.GeospatialKMeansModel(KMeansModel.load(path))
  }
}

class GeospatialKMeansModel(private val _model: KMeansModel)(implicit spark: SparkSession)
  extends BaseGeospatialClusteringModel {

  override protected def transformInternal(df: DataFrame): DataFrame = _model.transform(df);

  override def k: Int = _model.getK

  def underlyingModel: KMeansModel = _model

  override def save(path: String): Unit = _model.save(path)
}

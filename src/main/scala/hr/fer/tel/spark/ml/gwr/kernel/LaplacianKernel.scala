package hr.fer.tel.spark.ml.gwr.kernel

class LaplacianKernel extends Kernel1D {
  override def transform(value: Double, bandwidth: Double): Double = if (bandwidth == 0) 1 else math.exp(-math.abs(value / bandwidth))
}

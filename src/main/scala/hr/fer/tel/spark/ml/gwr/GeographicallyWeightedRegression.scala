package hr.fer.tel.spark.ml.gwr

import hr.fer.tel.spark.ml.gwr.kernel.{Kernel1D, UniformKernel}
import hr.fer.tel.spark.ml.gwr.neighbourhood.{DistanceBasedNeighbourhood, KernelNeighbourhood}
import org.apache.spark.ml.param._
import org.apache.spark.ml.regression.GeneralizedLinearRegression
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.ml.{Estimator, Model}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.types.{DoubleType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

trait HasGeometryCol extends Params {
  def geometryCol: Param[String] = new Param[String](this, "geometryCol", "Geometry column.")

  setDefault(ParamPair(geometryCol, "geometry"))

  def getGeometryCol: String = $(geometryCol)
}

trait HasGeneralizedLinearRegression extends Params {
  def generalizedLinearRegression: Param[GeneralizedLinearRegression] = new Param[GeneralizedLinearRegression](
    this, "generalisedLinearRegression", "GeneralizedLinearRegression used as underlying Regressor implementation."
  )

  setDefault(generalizedLinearRegression, new GeneralizedLinearRegression())

  def getGeneralizedLinearRegression: GeneralizedLinearRegression = $(generalizedLinearRegression)
}

trait GeographicallyWeightedRegressionParams extends Params
  with HasGeometryCol with HasGeneralizedLinearRegression {

  def kernelNeighbourhood: Param[KernelNeighbourhood] = new Param[KernelNeighbourhood](
    this, "kernelNeighbourhood", "KernelNeighbourhood implementation used for finding train sets for fitting local Regressors."
  )

  setDefault(kernelNeighbourhood, new DistanceBasedNeighbourhood(bandwidthKm = Double.MaxValue))

  def getKernelNeighbourhood: KernelNeighbourhood = $(kernelNeighbourhood)

  def kernel: Param[Kernel1D] = new Param[Kernel1D](
    this, "kernel", "Kernel1D implementation used for calculating weights of each record in local training set."
  )

  setDefault(kernel, new UniformKernel())

  def getKernel: Kernel1D = $(kernel)

  def maxInputSize: IntParam = new IntParam(this, "maxInputSize", "Maximum input size.", { (x: Int) => x > 0 })

  setDefault(maxInputSize, 1000)

  def getMaxInputSize: Int = $(maxInputSize)

  def maxJobDuration: Param[Duration] = new Param[Duration](this, "maxJobDuration", "Maximum job duration")

  setDefault(maxJobDuration, Duration.Inf)

  def getMaxJobDuration: Duration = $(maxJobDuration)
}

object GeographicallyWeightedRegression {
  private[gwr] val trainView = "traindf"
  private[gwr] val unknownView = "unknowndf"
  private[gwr] val weightCol = "weight"
  private[gwr] val kernelFunc = "kernel"
  private[gwr] val rowNumberCol = "rn"
}

// NOTE: This implementation isn't scalable because it requires too much tasks and
// cannot transform (predict) on datasets which don't fit in the memory of the master node
class GeographicallyWeightedRegression(override val uid: String)(implicit spark: SparkSession)
  extends Estimator[GeographicallyWeightedRegressionModel]
    with Serializable with GeographicallyWeightedRegressionParams {

  import GeographicallyWeightedRegression._

  def this()(implicit spark: SparkSession) = this(Identifiable.randomUID("gwr"))(spark)

  def setGeometryCol(value: String): GeographicallyWeightedRegression = set(geometryCol, value)

  def setKernelNeighbourhood(value: KernelNeighbourhood): GeographicallyWeightedRegression = set(kernelNeighbourhood, value)

  def setKernel(value: Kernel1D): GeographicallyWeightedRegression = set(kernel, value)

  def setMaxInputSize(value: Int): GeographicallyWeightedRegression = set(maxInputSize, value)

  def setMaxJobDuration(value: Duration): GeographicallyWeightedRegression = set(maxJobDuration, value)

  def setGeneralizedLinearRegression(value: GeneralizedLinearRegression): GeographicallyWeightedRegression = {
    value.setWeightCol(weightCol)
    set(generalizedLinearRegression, value)
  }

  override def fit(dataset: Dataset[_]): GeographicallyWeightedRegressionModel = {
    if (dataset == null) {
      throw new IllegalArgumentException("Input should not be null.")
    }

    new GeographicallyWeightedRegressionModel(dataset.toDF())
      .setGeometryCol(getGeometryCol)
      .setKernelNeighbourhood(getKernelNeighbourhood)
      .setKernel(getKernel)
      .setMaxInputSize(getMaxInputSize)
      .setMaxJobDuration(getMaxJobDuration)
      .setGeneralizedLinearRegression(getGeneralizedLinearRegression)
  }

  override def copy(extra: ParamMap): Estimator[GeographicallyWeightedRegressionModel] = {
    new GeographicallyWeightedRegression(uid)
      .setGeometryCol(extra.getOrElse(geometryCol, getGeometryCol))
      .setKernelNeighbourhood(extra.getOrElse(kernelNeighbourhood, getKernelNeighbourhood))
      .setKernel(extra.getOrElse(kernel, getKernel))
      .setMaxInputSize(extra.getOrElse(maxInputSize, getMaxInputSize))
      .setMaxJobDuration(extra.getOrElse(maxJobDuration, getMaxJobDuration))
      .setGeneralizedLinearRegression(extra.getOrElse(generalizedLinearRegression, getGeneralizedLinearRegression))
  }

  override def transformSchema(schema: StructType): StructType = schema
}

class GeographicallyWeightedRegressionModel private[gwr](override val uid: String,
                                                         private val _trainDf: DataFrame)(implicit spark: SparkSession)
  extends Model[GeographicallyWeightedRegressionModel] with GeographicallyWeightedRegressionParams with Serializable {

  import GeographicallyWeightedRegression._

  private[gwr] def this(trainDf: DataFrame)(implicit spark: SparkSession) =
    this(Identifiable.randomUID("gwrModel"), trainDf)(spark)

  def setGeometryCol(value: String): GeographicallyWeightedRegressionModel = set(geometryCol, value)

  def setKernelNeighbourhood(value: KernelNeighbourhood): GeographicallyWeightedRegressionModel = set(kernelNeighbourhood, value)

  def setKernel(value: Kernel1D): GeographicallyWeightedRegressionModel = set(kernel, value)

  def setMaxInputSize(value: Int): GeographicallyWeightedRegressionModel = set(maxInputSize, value)

  def setMaxJobDuration(value: Duration): GeographicallyWeightedRegressionModel = set(maxJobDuration, value)

  def setGeneralizedLinearRegression(value: GeneralizedLinearRegression): GeographicallyWeightedRegressionModel = {
    value.setWeightCol(weightCol)
    set(generalizedLinearRegression, value)
  }


  def transform(dataset: Dataset[_]): DataFrame = {
    if (dataset == null) {
      throw new IllegalArgumentException("Input should not be null.")
    }

    val df = dataset.toDF()

    import org.apache.spark.sql.functions.{lit, row_number}
    import spark.implicits._

    _trainDf.createOrReplaceTempView(trainView)

    val w = Window.orderBy(lit("A"))
    var tempDf = df.withColumn(rowNumberCol, row_number().over(w))

    tempDf.createOrReplaceTempView(unknownView)
    spark.udf.register(kernelFunc, (value: Double, bandwidth: Double) => getKernel.transform(value, bandwidth))

    tempDf = getKernelNeighbourhood.find(
      tempDf,
      geometryCol = getGeometryCol,
      featuresCol = getGeneralizedLinearRegression.getFeaturesCol,
      labelCol = getGeneralizedLinearRegression.getLabelCol,
      weightCol = weightCol,
      rowNumberCol = rowNumberCol,
      kernelName = kernelFunc,
      trainView = trainView,
      unknownView = unknownView
    )
    tempDf.persist()

    val inputCount = df.count()

    if (inputCount > getMaxInputSize.toLong) {
      throw new IllegalArgumentException(s"Input is too big.")
    }

    val hasLabelCol = df.columns.contains(s"${getGeneralizedLinearRegression.getLabelCol}")

    val predictRows = (1 to inputCount.toInt) map { rn =>
      Future {
        val glrLocal = getGeneralizedLinearRegression.copy(ParamMap.empty)

        val sampleDf = tempDf.filter($"${unknownView}_$rowNumberCol" === rn)
        val trainDf = sampleDf.selectExpr(
          s"${trainView}_${getGeneralizedLinearRegression.getFeaturesCol} AS ${getGeneralizedLinearRegression.getFeaturesCol}",
          s"${trainView}_${getGeneralizedLinearRegression.getLabelCol} AS ${getGeneralizedLinearRegression.getLabelCol}",
          "weight"
        )

        val testSample = if (hasLabelCol) {
          sampleDf.selectExpr(
            s"${unknownView}_${getGeneralizedLinearRegression.getFeaturesCol} AS ${getGeneralizedLinearRegression.getFeaturesCol}",
            s"${unknownView}_${getGeneralizedLinearRegression.getLabelCol} AS ${getGeneralizedLinearRegression.getLabelCol}",
            s"${unknownView}_$getGeometryCol AS $getGeometryCol"
          ).first()
        } else {
          sampleDf.selectExpr(
            s"${unknownView}_${getGeneralizedLinearRegression.getFeaturesCol} AS ${getGeneralizedLinearRegression.getFeaturesCol}",
            s"${unknownView}_$getGeometryCol AS $getGeometryCol"
          ).first()
        }
        val testDf = spark.createDataFrame(spark.sparkContext.parallelize(Seq(testSample)), testSample.schema)

        val predictDf = glrLocal.fit(trainDf)
          .transform(testDf)

        predictDf.first()
      }(ExecutionContext.global)
    } map {
      job => Await.ready(job, getMaxJobDuration)
    } map {
      job => Await.result(job, getMaxJobDuration)
    }

    tempDf.unpersist()
    spark.createDataFrame(spark.sparkContext.makeRDD[Row](predictRows), predictRows.head.schema)
  }

  override def copy(extra: ParamMap): GeographicallyWeightedRegressionModel = {
    new GeographicallyWeightedRegressionModel(uid, _trainDf)
      .setGeometryCol(extra.getOrElse(geometryCol, getGeometryCol))
      .setKernelNeighbourhood(extra.getOrElse(kernelNeighbourhood, getKernelNeighbourhood))
      .setKernel(extra.getOrElse(kernel, getKernel))
      .setMaxInputSize(extra.getOrElse(maxInputSize, getMaxInputSize))
      .setMaxJobDuration(extra.getOrElse(maxJobDuration, getMaxJobDuration))
      .setGeneralizedLinearRegression(extra.getOrElse(generalizedLinearRegression, getGeneralizedLinearRegression))
  }

  override def transformSchema(schema: StructType): StructType =
    schema.add(StructField(getGeneralizedLinearRegression.getPredictionCol, DoubleType))
}

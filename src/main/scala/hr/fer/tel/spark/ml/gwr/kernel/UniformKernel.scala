package hr.fer.tel.spark.ml.gwr.kernel

class UniformKernel extends Kernel1D {

  override def transform(value: Double, bandwidth: Double): Double = 1
}

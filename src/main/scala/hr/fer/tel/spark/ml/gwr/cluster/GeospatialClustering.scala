package hr.fer.tel.spark.ml.gwr.cluster

import org.apache.spark.sql.{DataFrame, SparkSession}

trait GeospatialClustering {

  def fit(df: DataFrame,
          prepared: Boolean = false,
          geometryCol: String = "geometry",
          centroidCol: String = "centroid",
          clusterCol: String = "cluster"): GeospatialClusteringModel
}

trait GeospatialClusteringModelLoader {
  def load(path: String)(implicit spark: SparkSession): GeospatialKMeansModel
}

trait GeospatialClusteringModel {

  def transform(df: DataFrame,
                prepared: Boolean = false,
                geometryCol: String = "geometry",
                centroidCol: String = "centroid"): DataFrame

  def k: Int

  def save(path: String): Unit
}

package hr.fer.tel.spark.ml.gwr.cluster.dbscan

import java.util

import org.apache.spark.broadcast.Broadcast
import org.apache.spark.internal.Logging
import org.apache.spark.ml.param.{Param, ParamMap, Params}
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{LongType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Dataset, Encoders, Row, SparkSession}
import org.locationtech.jts.geom.{Coordinate, Envelope}
import org.locationtech.jts.index.hprtree.HPRtree

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.mutable.Queue

trait DBScanParams extends Params {
  def eps: Param[Double] = new Param[Double](
    this, "eps", "maximum distance of points to e neighbours")

  def getEps: Double = $(eps)

  def minPts: Param[Integer] = new Param[Integer](
    this, "minPts", "number of points needed in neighbourhood for point to be core point")

  def getMinPts: Integer = $(minPts)

  def featuresCol: Param[String] = new Param[String](
    this, "featuresCol", "name of features colum in dataset")

  def getFeaturesCol: String = $(featuresCol)

  def predictionCol: Param[String] = new Param[String](
    this, "predictionCol", "predictionCol")

  def getPredictionCol: String = $(predictionCol)
}

class TreePoint(
                 val index: Long,
                 var partitionId : Long
               ) extends Serializable {}

class Point(
           val lat: Double,
           val lon: Double,
           val index: Long,
           var partitionId : Long
           ) extends Serializable {}

class DBScanModel(
                 val result: DataFrame
                 ) extends  Serializable {
  var k = 0
  def transform(dataset: DataFrame): DataFrame = {
    val res = dataset.alias("a").join(result.alias("b"), dataset("geometry") === result("geometry"), "inner").
      select("a.*", "b.cluster")
    k = res.groupBy("cluster").count().count().asInstanceOf[Int]
    res
  }
}

object PointsTree {
  var tree = new HPRtree()
  def broadcast(spark: SparkSession): Broadcast[HPRtree] ={
    val broadcast = spark.sparkContext.broadcast(tree)
    broadcast
  }
}

class DBScan private (
                       override val uid: String
                     )(implicit spark: SparkSession) extends Serializable with Logging with DBScanParams {

  def this()(implicit spark: SparkSession) = this(Identifiable.randomUID("dbscan"))(spark)

  def copy() : DBScan = new DBScan().setEps(getEps).setMinPts(getMinPts)

  override def copy(extra: ParamMap): DBScan = this

  def setEps(value: Double): DBScan = set(eps, value)
  def setMinPts(value: Integer): DBScan = set(minPts, value)
  def setFeaturesCol(value: String): DBScan = set(featuresCol, value)
  def setPredictionCol(value: String): DBScan = set(predictionCol, value)

  def addColumnIndex(df: DataFrame) = {
    spark.sqlContext.createDataFrame(
      df.rdd.zipWithIndex.map {
        case (row, index) => Row.fromSeq(row.toSeq :+ index)
      },
      // Create schema for index column
      StructType(df.schema.fields :+ StructField("index", LongType, false)))
  }

  def fit(dataset: Dataset[_]): DBScanModel = {
    import spark.implicits._
    var points = dataset.withColumn("partitionID", spark_partition_id())
    points = addColumnIndex(points)
    val points2 = points.map(r => new Point(r.getDouble(r.fieldIndex(getFeaturesCol+"_x")), r.getDouble(r.fieldIndex(getFeaturesCol+"_y")),
      r.getAs[Long]("index"), r.getAs[Int]("partitionID")))(Encoders.kryo[Point])

    points2.collect().foreach(p=>{
      PointsTree.tree.insert(new Envelope(new Coordinate(p.lat, p.lon)), p)
    })
    val brTree = PointsTree.broadcast(spark)
    createPartialClusters(points2, brTree)
    val result2 = mergePartialClusters()
    var result = result2.toSeq.toDF("index", "cluster")
    result = result.alias("a").join(points.alias("b"), result("index") === points("index"), "inner").
      select("b.*", "a.cluster")
    val model = new DBScanModel(result)
    model
  }

  val clusterAccum = new ClusterAccumulator()
  spark.sparkContext.register(clusterAccum, "Partial clusters")

  val accumNoise = spark.sparkContext.longAccumulator("Noise")
  val accumNoise2 = spark.sparkContext.longAccumulator("Noise2")
  val accumSeed = spark.sparkContext.longAccumulator("Not seed")
  val accumIsti = spark.sparkContext.longAccumulator("Isti")


  def createPartialClusters(data: Dataset[Point], brTree: Broadcast[HPRtree]) = { //DBScanModel
    val hashtable = mutable.HashSet.empty[Long]
    val noisetable = mutable.HashSet.empty[Long]

    data.foreach(f=>{
      val neighbours : Queue[Point] = Queue()
      if(!hashtable.contains(f.index)){
        //define query area for tree
        val envelope = new Envelope(f.lat-getEps, f.lat+getEps, f.lon-getEps, f.lon+getEps)
        //get points from defined query area
        val pointNeighbours = brTree.value.query(envelope).asInstanceOf[util.ArrayList[Point]]
        var nCount = 0
        for(e <- pointNeighbours.asScala){
          //add to neighbours points with distance less than Eps using Euclidian distance
          if (Math.sqrt(Math.pow((e.lat - f.lat), 2) + Math.pow((e.lon - f.lon), 2)) <= getEps) {
            neighbours.enqueue(e)
            nCount += 1
          }
        }
        if(nCount<getMinPts){
          noisetable.add(f.index)
          accumNoise.add(1)
        }
        else{
          val seentable = mutable.HashSet.empty[Long]
          var cluster : mutable.HashMap[Long, Boolean] = mutable.HashMap()
          cluster += (f.index -> false)
          hashtable.add(f.index)
          seentable.add(f.index)
          var seed_flg = false
          val parInd: Long = f.partitionId

          while(neighbours.size>0){
            val p = neighbours.dequeue()
            var seed = false

            if (p.partitionId != parInd) {
              seed = true
            }
            else{
              seed = false
            }
            if(!hashtable.contains(p.index) || (seed == true && seed_flg == false)){
              if(true) {
                hashtable.add(p.index)
              }
              if (!seentable.contains(p.index)) {
                seentable.add(p.index)
                if(noisetable.contains(p.index)){
                  accumNoise2.add(1)
                }
                val neighbours2: Queue[Point] = Queue()
                var nPts = 0
                val envelope2 = new Envelope(p.lat - getEps, p.lat + getEps, p.lon - getEps, p.lon + getEps)
                val pointNeighbours2 = brTree.value.query(envelope2).asInstanceOf[util.ArrayList[Point]]
                for (n <- pointNeighbours2.asScala) {
                  if (Math.sqrt(Math.pow((n.lat - p.lat), 2) + Math.pow((n.lon - p.lon), 2)) <= getEps) {
                    nPts += 1
                    neighbours2.enqueue(n)
                  }
                }
                if (nPts >= getMinPts) {
                  neighbours2.foreach(h => {
                    neighbours.enqueue(h)
                  })
                }
                if (!cluster.contains(p.index)) {
                  cluster += (p.index -> seed)
                  if(seed){
                    seed_flg = true
                  }
                }
              }
            }
          }
          clusterAccum.add(new PartialCluster(cluster, 0))
        }
      }
    })
    print("Noise: "+accumNoise.value+"\n")
    print("Preobraćeni Noise: "+accumNoise2.value+"\n")
  }

  def mergePartialClusters(): mutable.HashMap[Long, Long] = {
    var fullClusters: mutable.HashMap[Long, Long] = mutable.HashMap()
    var i = 0
    print("\nParcijalnih: "+clusterAccum.clusters.size+"\n")
    for(c <- clusterAccum.clusters){
      if(c.status == 0){
        //find seeds in partial cluster (p => (index -> seed))
        val seeds = c.cluster.filter(p => p._2 == true).toSeq
        var flag = false
        for(s <- seeds) {
          for(cc <- clusterAccum.clusters){
            if(c != cc) {
              //if partial cluster contains current seed point as not seed
              if(cc.cluster.contains(s._1) && cc.status == 0){
                val pp = cc.cluster.get(s._1)
                if(!pp.get) {
                  flag = true
                  //add child partial cluster non seed points to full cluster
                  for(p <- cc.cluster){
                    if(!p._2) {
                      fullClusters += (p._1 -> i)
                    }
                    else{
                      seeds :+ (p._1, p._2)
                    }
                  }
                  cc.status = 1
                }
              }
            }
          }
        }
        if(flag){
          for(s <- seeds){
            if(!fullClusters.contains(s._1)){
              fullClusters += (s._1 -> i)
            }
          }
        }
        if(!flag){
          var j = 0
          var flag2 = true
          val seeds2 = seeds.toVector
          if(seeds2.size > 0) {
            while (flag2 && j<seeds2.size) {
              val seed = seeds2(j)
              if(fullClusters.contains(seed._1)){
                for(p <- c.cluster){
                  if(!p._2){
                    fullClusters += (p._1 -> fullClusters.get(seed._1).get)
                  }
                  else{
                    if(!fullClusters.contains(p._1)){
                      fullClusters += (p._1 -> fullClusters.get(seed._1).get)
                    }
                  }
                }
                flag2 = false
              }
              j+=1
            }
          }
          if(flag2){
            for(p<-c.cluster){
              fullClusters += (p._1 -> i)
            }
            i += 1
          }
        }
        else{
          //add parent partial cluster points to full cluster
          for(p<-c.cluster){
            if(!p._2) {
              fullClusters += (p._1 -> i)
            }
            else{
              if(!fullClusters.contains(p._1)){
                fullClusters += (p._1 -> i)
              }
            }
          }
          i += 1
        }
        c.status = 1
      }
    }
    print("Spojenih: "+i+"\n")
    print("Na izlazu: " + fullClusters.size+"\n")

    fullClusters
  }
}
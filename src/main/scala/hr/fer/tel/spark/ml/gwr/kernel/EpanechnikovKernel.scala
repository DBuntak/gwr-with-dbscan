package hr.fer.tel.spark.ml.gwr.kernel

class EpanechnikovKernel extends Kernel1D {
  override def transform(value: Double, bandwidth: Double): Double = if (bandwidth == 0) 1 else (1 - (math.pow(value, 2)/math.pow(bandwidth, 2)))
}

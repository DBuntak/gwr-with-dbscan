package hr.fer.tel.spark.ml.gwr

import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.types.{DoubleType, StructType}
import org.apache.spark.sql.{DataFrame, SparkSession}


object ZillowDataLoader {

  val FeatureColumns: Array[String] = Array(
    "taxamount",
    "taxvaluedollarcnt",
    "landtaxvaluedollarcnt",
    "structuretaxvaluedollarcnt",
    "finishedsquarefeet12",
    "calculatedfinishedsquarefeet",
    "calculatedbathnbr",
    "fullbathcnt",
    "bathroomcnt",
    "bedroomcnt",
    "yearbuilt"
  )

  val ZillowSchema: StructType = new StructType()
    .add("taxamount", DoubleType, nullable = true)
    .add("taxvaluedollarcnt", DoubleType, nullable = true)
    .add("landtaxvaluedollarcnt", DoubleType, nullable = true)
    .add("structuretaxvaluedollarcnt", DoubleType, nullable = true)
    .add("finishedsquarefeet12", DoubleType, nullable = true)
    .add("calculatedfinishedsquarefeet", DoubleType, nullable = true)
    .add("calculatedbathnbr", DoubleType, nullable = true)
    .add("fullbathcnt", DoubleType, nullable = true)
    .add("bathroomcnt", DoubleType, nullable = true)
    .add("bedroomcnt", DoubleType, nullable = true)
    .add("yearbuilt", DoubleType, nullable = true)
    .add("longitude", DoubleType, nullable = true)
    .add("latitude", DoubleType, nullable = true)
    .add("logerror", DoubleType, nullable = true)

  def load(inputLocation: String)(implicit spark: SparkSession): DataFrame = {
    val df = spark
      .read
      .format("csv")
      .option("header", "true")
      .schema(ZillowSchema)
      .load(inputLocation)

    new VectorAssembler()
      .setInputCols(FeatureColumns)
      .setOutputCol("features")
      .transform(df.na.drop)
      .selectExpr(
        "ST_Point(CAST(longitude AS Decimal(24,20)), CAST(latitude AS Decimal(24,20))) AS geometry",
        "logerror AS label",
        "features"
      )
  }
}

package hr.fer.tel.spark.ml.gwr.cluster.dbscan

import java.io.FileInputStream
import java.util.Properties

import hr.fer.tel.spark.ml.gwr.ZillowDataLoader
import hr.fer.tel.spark.ml.gwr.cluster.CentroidFinder
import org.apache.spark.ml.feature.StandardScaler
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.sql.functions.spark_partition_id
import org.apache.spark.sql.types.{LongType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Encoders, Row, SparkSession}
import org.datasyslab.geospark.serde.GeoSparkKryoRegistrator
import org.datasyslab.geosparksql.utils.GeoSparkSQLRegistrator
import org.locationtech.jts.geom.{Coordinate, Envelope}
import org.locationtech.jts.index.hprtree.HPRtree

import scala.collection.JavaConverters._
import scala.util.Random

object DBScanMain extends App {

  def createSparkSession(props: Properties): SparkSession = {
    val builder = SparkSession
      .builder()
      .appName(props.getOrDefault("spark.appName", "GWR").toString)
      .master(props.getOrDefault("spark.masterUrl", configs("sparkMaster")).toString)
      .config("spark.serializer", classOf[KryoSerializer].getName)
      .config("spark.kryo.registrator", classOf[GeoSparkKryoRegistrator].getName)
      .config("geospark.join.gridtype", "rtree")

    builder.config("geospark.global.index", value = true)
    builder.config("geospark.global.indextype", "rtree")

    val numOfPartitions = props.get("geospark.join.numpartition")
    if (numOfPartitions != null) {
      builder.config("geospark.join.numpartition", numOfPartitions.asInstanceOf[Int])
    }

    builder.getOrCreate()
  }


  if (args.length != 1) {
    throw new IllegalArgumentException("Program accepts exactly one argument: configuration file path.")
  }

  val props = new Properties()
  props.load(new FileInputStream(args(0)))
  val configs = props.asScala

  println("Received configuration:")
  for ((k, v) <- configs) printf("%s=%s\n", k, v)

  implicit val spark: SparkSession = createSparkSession(props)
  GeoSparkSQLRegistrator.registerAll(spark)

  val df: DataFrame = ZillowDataLoader.load(configs("inputLocation"))

  val regParam = configs("glr.regressionParam").toDouble
  val maxIter = configs("glr.maxIter").toInt
  val convergenceTol = configs("glr.convergenceTol").toDouble
  val numOfClusters = configs("gcr.numOfClusters").toInt
  val numOfNeighbours = configs("gwr.numOfNeighbours").toInt
  val bandwidthKm = configs("gwr.bandwidthKm").toDouble
  val testSplit = configs("testSplit").toDouble
  val randomSeed = configs.get("randomSeed") match {
    case Some(value: String) => value.toInt
    case None => new Random().nextInt()
  }

  println(s"randomSeed=$randomSeed\n")

  if (testSplit <= 0.0 || testSplit > 0.3) {
    throw new IllegalArgumentException("Train split must be between 0 and 0.3!")
  }

  val scaler = new StandardScaler()
    .setInputCol("features")
    .setOutputCol("scaledFeatures")
    .setWithStd(true)
    .setWithMean(true)

  val Array(train, test) = scaler
    .fit(df)
    .transform(df)
    //.randomSplit(Array(1 - testSplit, testSplit), seed = randomSeed)
    .randomSplit(Array(0.998, 0.002), seed = -815689481)

  val numSamples = test.count().toInt

  val t6 = System.currentTimeMillis()
  def addColumnIndex(df: DataFrame) = {
    spark.sqlContext.createDataFrame(
      df.rdd.zipWithIndex.map {
        case (row, index) => Row.fromSeq(row.toSeq :+ index)
      },
      // Create schema for index column
      StructType(df.schema.fields :+ StructField("index", LongType, false)))
  }

  val train2 = CentroidFinder.find(train, "geometry", "centroid")
  val train22 = train2.withColumn("partitionID", spark_partition_id())
  val train222 = addColumnIndex(train22)

  val train3 = train222.map(r => new Point(r.getDouble(4), r.getDouble(5), r.getLong(8),
    r.getInt(7)))(Encoders.kryo[Point])

  val tree = new HPRtree()

  val t0 = System.currentTimeMillis()
  train3.collect.foreach(p=>{
    tree.insert(new Envelope(new Coordinate(p.lat, p.lon)), p)
  })
  tree.build()
  val t1 = System.currentTimeMillis()
  print("Stablo: "+tree.size()+"\n")
  val brTree = spark.sparkContext.broadcast(tree)

  val dbscan = new DBScan()
  val t2 = System.currentTimeMillis()
  dbscan.createPartialClusters(train3,brTree)
  val t3 = System.currentTimeMillis()

  val t4 = System.currentTimeMillis()
  val result2 = dbscan.mergePartialClusters()
  val t5 = System.currentTimeMillis()
  import spark.implicits._
  val result22  = result2.toSeq.toDF("index", "cluster")

  val result3 = result22.join(train222, result22("index") === train222("index"), "inner")
  val t7 = System.currentTimeMillis()

  print("Broj točaka: "+train.count()+"\n")
  print("Krajnji broj točaka: "+result3.count()+"\n")
  result3.groupBy("cluster").count().show(50)
  println("Vrijeme stabla " + (t1 - t0) + " Millis")
  println("Vrijeme runa " + (t3 - t2) + " Millis")
  println("Vrijeme mergea " + (t5 - t4) + " Millis")
  println("Vrijeme algoritma " + (t7 - t6) + " Millis")
  import java.io._
  val pw = new PrintWriter(new File("C:\\Users\\David\\Desktop\\tocke.txt" ))
  result3.foreach(r=> {
    pw.write(r.getDouble(6) + " " + r.getDouble(7) + " " + r.getLong(1) + "\n")
  })
  pw.close
}

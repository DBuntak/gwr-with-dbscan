package hr.fer.tel.spark.ml.gwr.cluster.meanshift

import java.io.FileInputStream
import java.util.Properties

import hr.fer.tel.spark.ml.gwr.ZillowDataLoader
import hr.fer.tel.spark.ml.gwr.cluster.CentroidFinder
import hr.fer.tel.spark.ml.gwr.cluster.dbscan.DBScanMain.{args, configs, createSparkSession}
import org.apache.spark.ml.feature.StandardScaler
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.sql.types.{IntegerType, StructField}
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.datasyslab.geospark.serde.GeoSparkKryoRegistrator
import org.datasyslab.geosparksql.utils.GeoSparkSQLRegistrator

import scala.util.Random
import scala.collection.JavaConverters._

object MeanShiftMain extends App {

  def createSparkSession(props: Properties): SparkSession = {
    val builder = SparkSession
      .builder()
      .appName(props.getOrDefault("spark.appName", "GWR").toString)
      .master(props.getOrDefault("spark.masterUrl", configs("sparkMaster")).toString)
      .config("spark.serializer", classOf[KryoSerializer].getName)
      .config("spark.kryo.registrator", classOf[GeoSparkKryoRegistrator].getName)
      .config("geospark.join.gridtype", "rtree")

    builder.config("geospark.global.index", value = true)
    builder.config("geospark.global.indextype", "rtree")

    val numOfPartitions = props.get("geospark.join.numpartition")
    if (numOfPartitions != null) {
      builder.config("geospark.join.numpartition", numOfPartitions.asInstanceOf[Int])
    }

    builder.getOrCreate()
  }

  if (args.length != 1) {
    throw new IllegalArgumentException("Program accepts exactly one argument: configuration file path.")
  }

  val props = new Properties()
  props.load(new FileInputStream(args(0)))
  val configs = props.asScala

  println("Received configuration:")
  for ((k, v) <- configs) printf("%s=%s\n", k, v)

  implicit val spark: SparkSession = createSparkSession(props)
  GeoSparkSQLRegistrator.registerAll(spark)

  var df: DataFrame = ZillowDataLoader.load(configs("inputLocation"))

  val regParam = configs("glr.regressionParam").toDouble
  val maxIter = configs("glr.maxIter").toInt
  val convergenceTol = configs("glr.convergenceTol").toDouble
  val numOfClusters = configs("gcr.numOfClusters").toInt
  val numOfNeighbours = configs("gwr.numOfNeighbours").toInt
  val bandwidthKm = configs("gwr.bandwidthKm").toDouble
  val testSplit = configs("testSplit").toDouble
  val randomSeed = configs.get("randomSeed") match {
    case Some(value: String) => value.toInt
    case None => new Random().nextInt()
  }

  println(s"randomSeed=$randomSeed\n")

  if (testSplit <= 0.0 || testSplit > 0.3) {
    throw new IllegalArgumentException("Train split must be between 0 and 0.3!")
  }

  val scaler = new StandardScaler()
    .setInputCol("features")
    .setOutputCol("scaledFeatures")
    .setWithStd(true)
    .setWithMean(true)

  var Array(train, test) = scaler
    .fit(df)
    .transform(df)
    //.randomSplit(Array(1 - testSplit, testSplit), seed = randomSeed)
    .randomSplit(Array(0.998, 0.002), seed = 12345)

  val meanShift = msLsh.MsLsh
  var data = spark.sparkContext.textFile(configs("inputLocation"))
  val header = data.first()
  data = data.filter(row => row != header)

  train = CentroidFinder.find(train, "geometry", "centroid")

  val t4 = System.currentTimeMillis()

  val parsedData2 = train.rdd.map(x => Array(x.getDouble(4), x.getDouble(5))).map(y => (Vectors.dense(y)))
    .zipWithIndex
    .map(_.swap)


  val parsedData22 = train.rdd.zipWithIndex.map(_.swap)

  val models = meanShift.train(  spark.sparkContext,
    parsedData2,
    k=100,
    epsilon1=0.001,
    epsilon2=0.05,
    epsilon3=0.06,
    ratioToStop=0.01,
    yStarIter=10,
    cmin=0,
    normalisation=true,
    w=1,
    nbseg=200,
    nbblocs1=10,
    nbblocs2=10,
    nbLabelIter=1)

  val res = models.head.labelizedRDD.map{ case (clusterID, (id, vector)) => (id, clusterID) }
  val result = parsedData22.join(res)
  import org.apache.spark.sql._
  val result2 = result.map{ case (id, (row, cluster)) => Row.fromSeq(row.toSeq :+ cluster) }
  val result3 = spark.createDataFrame(result2, train.schema.add(StructField("cluster", IntegerType, true)))
  val t5 = System.currentTimeMillis()
  result3.groupBy("cluster").count().show(50)
  print("Broj točaka: "+train.count()+"\n")
  print("Krajnji broj točaka: "+result3.count()+"\n")
  println("Vrijeme izvođenja " + (t5 - t4) + " Millis")

  import java.io._
  val pw = new PrintWriter(new File("C:\\Users\\David\\Desktop\\tocke.txt" ))
  result3.foreach(r=> {
    pw.write(r.getDouble(4) + " " + r.getDouble(5) + " " + r.getInt(7) + "\n")
  })
  pw.close

  // Save result as (ID, ClusterNumber)
  meanShift.savelabeling(models.head, "MyResultDirectory", 1)

  // Save centroids result as (NumCluster, cardinality, CentroidVector)
  meanShift.saveClusterInfo(models.head, "centroidDirectory")
}

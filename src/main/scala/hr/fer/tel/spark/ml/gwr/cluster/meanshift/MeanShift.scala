package hr.fer.tel.spark.ml.gwr.cluster.meanshift

import org.apache.spark.ml.param.{Param, ParamMap, Params}
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.sql.types.{IntegerType, StructField}
import org.apache.spark.sql.{Dataset, Row, SparkSession}


trait MeanShiftParams extends Params {
  def k: Param[Integer] = new Param[Integer](
    this, "k", "number of neighbours to look at in order to compute centroid")

  def getK: Integer = $(k)

  def nbseg: Param[Integer] = new Param[Integer](
    this, "nbseg", "number of segments on which the data vectors are projected during LSH")

  def getNbseg: Integer = $(nbseg)

  def nbblocks1: Param[Integer] = new Param[Integer](
    this, "nbblocks1", "crucial parameter as larger values give faster but less accurate LSH " +
      "approximate nearest neighbors, and as smaller values give slower but more accurate approximations")

  def getNbblocks1: Integer = $(nbblocks1)

  def nbblocks2: Param[Integer] = new Param[Integer](
    this, "nbblocks2", "crucial parameter as larger values give faster but less accurate LSH " +
      "approximate nearest neighbors, and as smaller values give slower but more accurate approximations")

  def getNbblocks2: Integer = $(nbblocks2)

  def cmin: Param[Integer] = new Param[Integer](
    this, "cmin", "threshold under which clusters with fewer than cmin members are merged with the " +
      "next nearest cluster")

  def getCmin: Integer = $(cmin)

  def normalization: Param[Boolean] = new Param[Boolean](
    this, "normalization", "flag if the data should be first normalized (X-Xmin)/(Xmax-Xmin) " +
      "before clustering")

  def getNormalization: Boolean = $(normalization)


  def w: Param[Double] = new Param[Double](
    this, "w", "uniformisation constant for LSH")

  def getW: Double = $(w)

  def yStarIter: Param[Integer] = new Param[Integer](
    this, "yStarIter", "maximum number of iterations in the gradient ascent in the mean shift update")

  def getYStarIter: Integer = $(yStarIter)

  def nbLabelIter: Param[Integer] = new Param[Integer](
    this, "nbLabelIter", "number of iteration for the labelisation step, it determines the number " +
      "of final models")

  def getNbLabelIter: Integer = $(nbLabelIter)

  def ratioToStop: Param[Double] = new Param[Double](
    this, "ratioToStop", "percentage of data that need to not have converged in order to " +
      "considere to stop gradient ascent iterations, set to 1.0 to disable it")

  def getRatioToStop: Double = $(ratioToStop)

  def epsilon1: Param[Double] = new Param[Double](
    this, "epsilon1", "threshold under which we considere a mod to have converged")

  def getEpsilon1: Double = $(epsilon1)

  def epsilon2: Param[Double] = new Param[Double](
    this, "epsilon2", "threshold under which two final mean shift iterates are considered to " +
      "be in the same cluster")

  def getEpsilon2: Double = $(epsilon2)

  def epsilon3: Param[Double] = new Param[Double](
    this, "epsilon3", "threshold under which two final clusters are considered to be the same")

  def getEpsilon3: Double = $(epsilon3)


}

class MeanShiftModel(
                   var result:  msLsh.Mean_shift_lsh_model
                 )(implicit spark: SparkSession) extends  Serializable {
  import org.apache.spark.sql._
  var k = 0
  def transform(dataset: DataFrame): DataFrame = {
    //add index to given dataset
    val preparedData = dataset.rdd.zipWithIndex.map(_.swap)
    //map clustering results to (id, clusterID)
    val mappedResult = result.labelizedRDD.map{ case (clusterID, (id, vector)) => (id, clusterID) }
    //join clusterID with features
    val joined = preparedData.join(mappedResult)
    //prepare data for dataframe creation
    val prepareDataframe = joined.map{ case (id, (row, cluster)) => Row.fromSeq(row.toSeq :+ cluster) }
    val resultDataframe = spark.createDataFrame(prepareDataframe, dataset.schema.add(StructField("cluster", IntegerType, true)))
    k = result.numCluster
    resultDataframe
  }
}

class MeanShift(override val uid: String)(implicit spark: SparkSession) extends  Serializable with MeanShiftParams {

  def this()(implicit spark: SparkSession) = this(Identifiable.randomUID("meanshift"))(spark)

  def setK(value: Integer): MeanShift = set(k, value)
  def setNbseg(value: Integer): MeanShift = set(nbseg, value)
  def setNbblcks1(value: Integer): MeanShift = set(nbblocks1, value)
  def setNbblocks2(value: Integer): MeanShift = set(nbblocks2, value)
  def setCmin(value: Integer): MeanShift = set(cmin, value)
  def setW(value: Double): MeanShift = set(w, value)
  def setYStarIter(value: Integer): MeanShift = set(yStarIter, value)
  def setNbLabelIter(value: Integer): MeanShift = set(nbLabelIter, value)
  def setNormalization(value: Boolean): MeanShift = set(normalization, value)
  def setRatioToStop(value: Double): MeanShift = set(ratioToStop, value)
  def setEpsilon1(value: Double): MeanShift = set(epsilon1, value)
  def setEpsilon2(value: Double): MeanShift = set(epsilon2, value)
  def setEpsilon3(value: Double): MeanShift = set(epsilon3, value)

  override def copy(extra: ParamMap): MeanShift = {
    this
  }


  def fit(dataset: Dataset[_], centroidCol: String): MeanShiftModel = {
    //extract geospatial coordinates and add index to every row
    val preparedData = dataset.toDF().rdd.map(x => Array(x.getAs[Double](centroidCol+"_x"),
      x.getAs[Double](centroidCol+"_y"))).map(y => (Vectors.dense(y)))
      .zipWithIndex
      .map(_.swap)

    val meanShift = msLsh.MsLsh
    val models = meanShift.train(spark.sparkContext,
      preparedData,
      k=getK,
      epsilon1=getEpsilon1,
      epsilon2=getEpsilon2,
      epsilon3=getEpsilon3,
      ratioToStop=getRatioToStop,
      yStarIter=getYStarIter,
      cmin=getCmin,
      normalisation=getNormalization,
      w=getW,
      nbseg=getNbseg,
      nbblocs1=getNbblocks1,
      nbblocs2=getNbblocks2,
      nbLabelIter=getNbLabelIter)
    val model = new MeanShiftModel(models.head)
    model
  }
}

package hr.fer.tel.spark.ml.gwr.cluster.dbscan

import org.apache.spark.util.AccumulatorV2

import scala.collection.mutable
import scala.collection.mutable.Queue

class PartialCluster(
                      val cluster: mutable.HashMap[Long, Boolean],
                      var status: Int //0-unfinished, 1-finished
                    ) extends Serializable {}


class ClusterAccumulator(var clusters: mutable.Queue[PartialCluster]) extends AccumulatorV2[PartialCluster, mutable.Queue[PartialCluster]] {
  def this() = this(Queue())

  override def isZero: Boolean = clusters.isEmpty

  override def copy(): AccumulatorV2[PartialCluster, mutable.Queue[PartialCluster]] = {
    new ClusterAccumulator(clusters)
  }

  override def reset(): Unit = clusters = Queue()

  override def add(v: PartialCluster): Unit = clusters.enqueue(v)

  override def merge(other: AccumulatorV2[PartialCluster, mutable.Queue[PartialCluster]]): Unit = {
    other.value.foreach(f=>{
      clusters.enqueue(f)
    })
  }

  override def value: mutable.Queue[PartialCluster] = clusters
}

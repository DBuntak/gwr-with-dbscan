package hr.fer.tel.spark.ml.gwr.kernel

trait Kernel1D extends Serializable {

  def transform(value: Double, bandwidth: Double): Double
}

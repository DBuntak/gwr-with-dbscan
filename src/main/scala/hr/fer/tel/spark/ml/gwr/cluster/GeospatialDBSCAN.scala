package hr.fer.tel.spark.ml.gwr.cluster

import hr.fer.tel.spark.ml.gwr.cluster.dbscan.{DBScan, DBScanModel}
import org.apache.spark.sql.{DataFrame, SparkSession}

class GeospatialDBSCAN(val templateModel: DBScan)(implicit spark: SparkSession)
  extends BaseGeospatialClustering {

  override protected def fitInternal(df: DataFrame,
                                     centroidCol: String,
                                     clusterCol: String): GeospatialClusteringModel = {
    val tempModel = templateModel.copy()
    tempModel.setFeaturesCol(centroidCol)
    tempModel.setPredictionCol(clusterCol)
    new GeospatialDBScanModel(tempModel.fit(df))
  }
}

class GeospatialDBScanModel(private val _model: DBScanModel)(implicit spark: SparkSession)
  extends BaseGeospatialClusteringModel {

  override protected def transformInternal(df: DataFrame): DataFrame = _model.transform(df)

  override def k: Int = _model.k

  def underlyingModel: DBScanModel = _model

  override def save(path: String): Unit = print("Not implemented")
}

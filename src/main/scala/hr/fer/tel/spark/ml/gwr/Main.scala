package hr.fer.tel.spark.ml.gwr

import java.io.FileInputStream
import java.util.Properties

import hr.fer.tel.spark.ml.gwr.cluster.{GeospatialDBSCAN, GeospatialKMeans, GeospatialMeanShift}
import hr.fer.tel.spark.ml.gwr.cluster.dbscan.DBScan
import hr.fer.tel.spark.ml.gwr.cluster.meanshift.MeanShift
import hr.fer.tel.spark.ml.gwr.kernel.{BiSquareKernel, BoxCarKernel, CosineKernel, EpanechnikovKernel, ExponentialKernel, GaussianKernel, LaplacianKernel}
import hr.fer.tel.spark.ml.gwr.neighbourhood.{DistanceBasedNeighbourhood, KNNBasedNeighbourhood, KernelNeighbourhood}
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.StandardScaler
import org.apache.spark.ml.regression.GeneralizedLinearRegression
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.datasyslab.geospark.serde.GeoSparkKryoRegistrator
import org.datasyslab.geosparksql.utils.GeoSparkSQLRegistrator

import scala.collection.JavaConverters._
import scala.util.Random


object Main extends App {

  def createSparkSession(props: Properties): SparkSession = {
    val builder = SparkSession
      .builder()
      .appName(props.getOrDefault("spark.appName", "GWR").toString)
      .master(props.getOrDefault("spark.masterUrl", configs("sparkMaster")).toString)
      .config("spark.serializer", classOf[KryoSerializer].getName)
      .config("spark.kryo.registrator", classOf[GeoSparkKryoRegistrator].getName)
      .config("spark.driver.memory", "4g")
      .config("geospark.join.gridtype", "rtree")

    builder.config("geospark.global.index", value = true)
    builder.config("geospark.global.indextype", "rtree")

    val numOfPartitions = props.get("geospark.join.numpartition")
    if (numOfPartitions != null) {
      builder.config("geospark.join.numpartition", numOfPartitions.asInstanceOf[Int])
    }

    builder.getOrCreate()
  }

  def time[R](block: => R): (R, Double) = {
    val t0 = System.nanoTime()
    val result = block
    val t1 = System.nanoTime()

    (result, (t1 - t0) * 1d / 1000000000)
  }

  def evaluateModel(transformFunction: DataFrame => DataFrame): (Double, Double, Double, Double, Double) = {
    val (predictedDf, transformTime) = time {
      transformFunction(test)
    }

    val rmse = rmseEvaluator.evaluate(predictedDf)
    val mse = mseEvaluator.evaluate(predictedDf)
    val r2 = r2Evaluator.evaluate(predictedDf)
    val mae = maeEvaluator.evaluate(predictedDf)

    (transformTime, rmse, mse, r2, mae)
  }

  def evaluateGLR: (String, Double, Double, Double, Double, Double, Double) = {
    val glr = new GeneralizedLinearRegression()
      .setFeaturesCol("scaledFeatures")
      .setRegParam(regParam)
      .setFamily("gaussian")
      .setLink("identity")
      .setMaxIter(maxIter)
      .setTol(convergenceTol)

    val (glrModel, fitTime) = time {
      glr.fit(train)
    }

    val (transformTime, rmse, mse, r2, mae) = evaluateModel(glrModel.transform)
    ("Generalised Linear Regression", fitTime, transformTime, rmse, mse, r2, mae)
  }

  def evaluateGWR(neighbourhood: KernelNeighbourhood): (Double, Double, Double, Double, Double, Double) = {
    val glr = new GeneralizedLinearRegression()
      .setFeaturesCol("scaledFeatures")
      .setWeightCol("weight")
      .setRegParam(regParam)
      .setFamily("gaussian")
      .setLink("identity")
      .setMaxIter(maxIter)
      .setTol(convergenceTol)

    val (gwrModel, fitTime) = time {
      new GeographicallyWeightedRegression()
        .setKernelNeighbourhood(neighbourhood)
        .setGeneralizedLinearRegression(glr)
        .setKernel(new BoxCarKernel())
        .setMaxInputSize(numSamples)
        .fit(train)
    }

    val (transformTime, rmse, mse, r2, mae) = evaluateModel(gwrModel.transform)
    (fitTime, transformTime, rmse, mse, r2, mae)
  }

  def evaluateGWRFixedKernel: (String, Double, Double, Double, Double, Double, Double) = {
    val result = evaluateGWR(
      new DistanceBasedNeighbourhood(bandwidthKm = bandwidthKm)
    )
    ("Geographically Weighted Regression (Fixed Kernel)", result._1, result._2, result._3, result._4, result._5, result._6)
  }

  def evaluateGWRVariableKernel: (String, Double, Double, Double, Double, Double, Double) = {
    val result = evaluateGWR(
      new KNNBasedNeighbourhood(numOfNeighbours)
    )
    ("Geographically Weighted Regression (Variable Kernel)", result._1, result._2, result._3, result._4, result._5, result._6)
  }

  def evaluateGCR: (String, Double, Double, Double, Double, Double, Double) = {
    val glr = new GeneralizedLinearRegression()
      .setFeaturesCol("scaledFeatures")
      .setRegParam(regParam)
      .setFamily("gaussian")
      .setLink("identity")
      .setMaxIter(maxIter)
      .setTol(convergenceTol)

    val (gcrModel, fitTime) = time {
      new GeographicallyClusteredRegression()
        .setGeospatialClustering(new GeospatialKMeans(new KMeans().setK(numOfClusters)))
        .setGeneralizedLinearRegression(glr)
        .fit(train)
    }

    val (transformTime, rmse, mse, r2, mae) = evaluateModel(gcrModel.transform)
    ("Geographically Clustered Regression", fitTime, transformTime, rmse, mse, r2, mae)
  }

  def evaluateGCRDBScan: (String, Double, Double, Double, Double, Double, Double) = {
    val glr = new GeneralizedLinearRegression()
      .setFeaturesCol("scaledFeatures")
      .setRegParam(regParam)
      .setFamily("gaussian")
      .setLink("identity")
      .setMaxIter(maxIter)
      .setTol(convergenceTol)

    val (gcrModel, fitTime) = time {
      new GeographicallyClusteredRegression()
        .setGeospatialClustering(new GeospatialDBSCAN(new DBScan().setEps(eps).setMinPts(minPts)))
        .setGeneralizedLinearRegression(glr)
        .fit(train)
    }

    val (transformTime, rmse, mse, r2, mae) = evaluateModel(gcrModel.transform)
    ("Geographically Clustered Regression", fitTime, transformTime, rmse, mse, r2, mae)
  }

  def evaluateGCRMeanShift: (String, Double, Double, Double, Double, Double, Double) = {
    val glr = new GeneralizedLinearRegression()
      .setFeaturesCol("scaledFeatures")
      .setRegParam(regParam)
      .setFamily("gaussian")
      .setLink("identity")
      .setMaxIter(maxIter)
      .setTol(convergenceTol)

    val (gcrModel, fitTime) = time {
      new GeographicallyClusteredRegression()
        .setGeospatialClustering(new GeospatialMeanShift(
          new MeanShift().setK(k)
        .setEpsilon1(epsilon1)
        .setEpsilon2(epsilon2)
        .setEpsilon3(epsilon3)
        .setRatioToStop(ratioToStop)
        .setYStarIter(yStarIter)
        .setCmin(cmin)
        .setNormalization(normalization)
        .setW(w)
        .setNbseg(nbseg)
        .setNbblcks1(nbblocks1)
        .setNbblocks2(nbblocks2)
        .setNbLabelIter(nbLabelIter)))
        .setGeneralizedLinearRegression(glr)
        .fit(train)
    }

    val (transformTime, rmse, mse, r2, mae) = evaluateModel(gcrModel.transform)
    ("Geographically Clustered Regression", fitTime, transformTime, rmse, mse, r2, mae)
  }

  if (args.length != 1) {
    throw new IllegalArgumentException("Program accepts exactly one argument: configuration file path.")
  }

  val props = new Properties()
  props.load(new FileInputStream(args(0)))
  val configs = props.asScala

  println("Received configuration:")
  for ((k, v) <- configs) printf("%s=%s\n", k, v)

  implicit val spark: SparkSession = createSparkSession(props)
  GeoSparkSQLRegistrator.registerAll(spark)

  val df: DataFrame = ZillowDataLoader.load(configs("inputLocation"))

  val regParam = configs("glr.regressionParam").toDouble
  val maxIter = configs("glr.maxIter").toInt
  val convergenceTol = configs("glr.convergenceTol").toDouble
  val numOfClusters = configs("gcr.numOfClusters").toInt
  val numOfNeighbours = configs("gwr.numOfNeighbours").toInt
  val bandwidthKm = configs("gwr.bandwidthKm").toDouble
  val testSplit = configs("testSplit").toDouble

  //mean shift params
  val k = configs("ms.k").toInt
  val nbblocks1 = configs("ms.nbblocks1").toInt
  val nbblocks2 = configs("ms.nbblocks2").toInt
  val yStarIter = configs("ms.yStarIter").toInt
  val cmin = configs("ms.cmin").toInt
  val nbseg = configs("ms.nbseg").toInt
  val nbLabelIter = configs("ms.nbLabelIter").toInt
  val epsilon1 = configs("ms.epsilon1").toDouble
  val epsilon2 = configs("ms.epsilon2").toDouble
  val epsilon3 = configs("ms.epsilon3").toDouble
  val w = configs("ms.w").toDouble
  val ratioToStop = configs("ms.ratioToStop").toDouble
  val normalization = configs("ms.normalization").toBoolean

  //dbscan params
  val minPts = configs("ds.minPts").toInt
  val eps = configs("ds.eps").toDouble

  val randomSeed = configs.get("randomSeed") match {
    case Some(value: String) => value.toInt
    case None => new Random().nextInt()
  }

  println(s"randomSeed=$randomSeed\n")

  if (testSplit <= 0.0 || testSplit > 0.3) {
    throw new IllegalArgumentException("Train split must be between 0 and 0.3!")
  }

  val scaler = new StandardScaler()
    .setInputCol("features")
    .setOutputCol("scaledFeatures")
    .setWithStd(true)
    .setWithMean(true)

  val Array(train, test) = scaler
    .fit(df)
    .transform(df)
    .randomSplit(Array(0.998, 0.002), seed = -815689481)

  val numSamples = test.count().toInt

  val rmseEvaluator = new RegressionEvaluator().setMetricName("rmse")
  val mseEvaluator = new RegressionEvaluator().setMetricName("mse")
  val r2Evaluator = new RegressionEvaluator().setMetricName("r2")
  val maeEvaluator = new RegressionEvaluator().setMetricName("mae")

  import spark.implicits._

  val evaluationDf = Seq(
      evaluateGLR
    , evaluateGWRFixedKernel
    , evaluateGWRVariableKernel
    , evaluateGCR,
    evaluateGCRDBScan,
    evaluateGCRMeanShift

  ).toDF("modelName", "fitTimeSeconds", "transformTimeSeconds",
    "rootMeanSquareError", "meanSquareError", "r^2", "meanAbsoluteError")

  evaluationDf.show()
  evaluationDf.write.csv(configs("outputLocation"))
}

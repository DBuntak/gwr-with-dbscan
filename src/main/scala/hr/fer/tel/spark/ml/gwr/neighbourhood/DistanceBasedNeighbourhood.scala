package hr.fer.tel.spark.ml.gwr.neighbourhood

import org.apache.spark.sql.DataFrame

class DistanceBasedNeighbourhood(val bandwidthKm: Double = 10.0) extends KernelNeighbourhood {

  // using approximation: 1 degree = 111 km
  private val bandwidth = bandwidthKm / 111.0

  override def find(df: DataFrame,
                    geometryCol: String = "geometry",
                    featuresCol: String = "features",
                    labelCol: String = "label",
                    weightCol: String = "weight",
                    rowNumberCol: String = "rn",
                    kernelName: String = "kernel",
                    trainView: String = "traindf",
                    unknownView: String = "unknowndf"): DataFrame = {
    val selectUnknownLabel = if (df.columns.contains(labelCol)) {
      (s"$unknownView.$labelCol AS ${unknownView}_$labelCol", s"${unknownView}_$labelCol")
    } else {
      ("", "")
    }

    val result = df.sqlContext.sql(
      s"""
         | SELECT $unknownView.$rowNumberCol AS ${unknownView}_$rowNumberCol,
         |        $unknownView.$featuresCol AS ${unknownView}_$featuresCol,
         |        $unknownView.$geometryCol AS ${unknownView}_$geometryCol,
         |        ${selectUnknownLabel._1},
         |        $trainView.$featuresCol AS ${trainView}_$featuresCol,
         |        $trainView.$labelCol AS ${trainView}_$labelCol,
         |        $kernelName(ST_Distance($unknownView.$geometryCol, $trainView.$geometryCol), $bandwidth) as $weightCol
         | FROM $unknownView CROSS JOIN $trainView
         | WHERE ST_Distance($unknownView.$geometryCol, $trainView.$geometryCol) <= $bandwidth
         |""".stripMargin
    )

    result
  }
}

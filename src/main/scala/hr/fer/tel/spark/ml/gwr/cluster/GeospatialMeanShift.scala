package hr.fer.tel.spark.ml.gwr.cluster

import hr.fer.tel.spark.ml.gwr.cluster
import hr.fer.tel.spark.ml.gwr.cluster.meanshift.{MeanShift, MeanShiftModel}
import org.apache.spark.ml.clustering.{KMeans, KMeansModel}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.sql.{DataFrame, SparkSession}

class GeospatialMeanShift(val templateModel: MeanShift)(implicit spark: SparkSession)
  extends BaseGeospatialClustering {

  override protected def fitInternal(df: DataFrame,
                                     centroidCol: String,
                                     clusterCol: String): GeospatialClusteringModel = {

    new GeospatialMeanShiftModel(templateModel.fit(df, centroidCol))
  }
}


class GeospatialMeanShiftModel(private val _model: MeanShiftModel)(implicit spark: SparkSession)
  extends BaseGeospatialClusteringModel {

  override protected def transformInternal(df: DataFrame): DataFrame = _model.transform(df);

  override def k: Int = _model.k

  def underlyingModel: MeanShiftModel = _model

  override def save(path: String): Unit = print("Not implemented")
}

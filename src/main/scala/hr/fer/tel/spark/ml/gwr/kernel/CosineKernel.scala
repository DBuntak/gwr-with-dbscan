package hr.fer.tel.spark.ml.gwr.kernel

class CosineKernel extends Kernel1D {
  override def transform(value: Double, bandwidth: Double): Double = if (math.abs(value) < bandwidth) math.cos((math.Pi*value)/(2*bandwidth)) else 0
}

package hr.fer.tel.spark.ml.gwr.examples

import java.io.File

import hr.fer.tel.spark.ml.gwr.cluster.{GeospatialKMeans, GeospatialKMeansModel}
import hr.fer.tel.spark.ml.gwr.{GeographicallyClusteredRegression, GeographicallyClusteredRegressionModel}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.datasyslab.geospark.serde.GeoSparkKryoRegistrator
import org.datasyslab.geosparksql.utils.GeoSparkSQLRegistrator

import scala.reflect.io.Directory

object GeographicallyClusteredRegressionExample extends App {

  Logger.getLogger("org").setLevel(Level.ERROR)
  Logger.getLogger("akka").setLevel(Level.ERROR)
  Logger.getRootLogger.setLevel(Level.ERROR)

  def createSparkSession: SparkSession = {
    val builder = SparkSession
      .builder()
      .appName("GCR")
      .master("local[*]")
      .config("spark.serializer", classOf[KryoSerializer].getName)
      .config("spark.kryo.registrator", classOf[GeoSparkKryoRegistrator].getName)
      .config("geospark.join.gridtype", "rtree")
      .config("spark.scheduler.mode", "FAIR")
      .config("spark.sql.warehouse.dir", "file:///C:/spark-warehouse/")
    builder.config("geospark.global.index", value = true)
    builder.config("geospark.global.indextype", "rtree")
    builder.getOrCreate()
  }

  def loadTrainingData()(implicit spark: SparkSession): DataFrame = {
    //TODO implement loading real data
    import spark.implicits._
    Seq(
      ("POINT(-88.341492 32.324242)", Vectors.dense(Seq(1.1, 2.5, 1.3).toArray), 2.5),
      ("POINT(-88.335592 32.324142)", Vectors.dense(Seq(2.1, 1.2, 1.2).toArray), 1.3),
      ("POINT(-88.331392 32.324742)", Vectors.dense(Seq(3.5, 5.2, 2.8).toArray), 1.7)
    ).toDF("geometry_str", "features", "label")
      .selectExpr("ST_GeomFromWKT(geometry_str) AS geometry", "features", "label")
  }

  def loadTestData()(implicit spark: SparkSession): DataFrame = {
    //TODO implement loading real data
    import spark.implicits._
    Seq(
      ("POINT(-88.331492 32.324242)", Vectors.dense(Seq(2.1, 3.5, 1.2).toArray), 1.5),
      ("POINT(-88.331592 32.324142)", Vectors.dense(Seq(1.1, 0.2, 1.7).toArray), 2.2),
      ("POINT(-88.331792 32.324742)", Vectors.dense(Seq(2.5, 1.2, 2.7).toArray), 0.4)
    ).toDF("geometry_str", "features", "label")
      .selectExpr("ST_GeomFromWKT(geometry_str) AS geometry", "features", "label")
  }

  implicit val spark: SparkSession = createSparkSession

  GeoSparkSQLRegistrator.registerAll(spark)

  var geoCol = "geometry"

  val traindf = loadTrainingData
  val unknowndf = loadTestData

  var gcr = new GeographicallyClusteredRegression()
    .setGeospatialClustering(new GeospatialKMeans(new KMeans().setK(3)))

  val model = gcr.fit(traindf)
  model.transform(unknowndf)
    .show()

  model.save("tmp_model")

  val model2 = GeographicallyClusteredRegressionModel
      .setClusteringModelLoader(GeospatialKMeansModel)
    .load("tmp_model")

  model2.transform(unknowndf)
    .show()

  new Directory(new File("tmp_model")).deleteRecursively()
}


# Geographically Weighted Regression in Spark ML

## Introduction

Geographically Weighted Regression (GWR) was developed in response to the finding that a regression model estimated over the entire area of interest may not adequately address local variations. The fairly simple principle on which it is based consists on estimating local models by least squares, each observation being weighted by a decreasing function of its distance to the estimation point. Combining these local models makes it possible to build a global model with specific properties. GWR can be used, in particular with the help of associated cartographic representations, to identify where local coefficients deviate the most from the overall coefficients, to build tests to assess whether the phenomenon is non-stationary and to characterise non-stationary. (Marie-Pierre de Bellefon & Jean-Michel Floch, Geographically Weighted Regression, INSEE, 2018.)
